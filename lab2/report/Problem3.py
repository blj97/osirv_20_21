import matplotlib.pyplot as plt
import cv2

image1 = cv2.imread("C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab2\\slike\\baboon.bmp",0)
image2 = cv2.imread("C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab2\\slike\\lenna.bmp",0)
image3 = cv2.imread("C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab2\\slike\\airplane.bmp",0)

image1 = cv2.cvtColor(image1, cv2.COLOR_BGR2RGB)
image2 = cv2.cvtColor(image2, cv2.COLOR_BGR2RGB)
image3 = cv2.cvtColor(image3, cv2.COLOR_BGR2RGB)

output1 = cv2.bitwise_not(image1)
output2 = cv2.bitwise_not(image2)
output3 = cv2.bitwise_not(image3)

cv2.imwrite("C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab2\\slike\\baboon_invert.bmp", output1)
cv2.imwrite("C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab2\\slike\\lenna_invert.bmp", output2)
cv2.imwrite("C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab2\\slike\\airplane_invert.bmp", output3)

plt.subplot(1, 2, 1)
plt.imshow(image1)
plt.title("Original")

plt.subplot(1, 2, 2)
plt.imshow(output1)
plt.title("Invert")

plt.show()

plt.subplot(1, 2, 1)
plt.imshow(image2)
plt.title("Original")

plt.subplot(1, 2, 2)
plt.imshow(output2)
plt.title("Invert")

plt.show()

plt.subplot(1, 2, 1)
plt.imshow(image3)
plt.title("Original")

plt.subplot(1, 2, 2)
plt.imshow(output3)
plt.title("Invert")

plt.show()