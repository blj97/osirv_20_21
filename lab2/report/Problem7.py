import cv2
from math import pi
import numpy as np
image = cv2.imread("C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab2\\slike\\baboon.bmp",0) 
nimage = cv2.resize(image,None,fx=0.25, fy=0.25) 
slika = nimage.copy()
deg =30
while deg <= 360:
    theta = (30 * pi) / 180
    M = cv2.getRotationMatrix2D((nimage.shape[1]/2, nimage.shape[0]/2), deg, 1)
    rotated = cv2.warpAffine(nimage.copy(), M, (nimage.shape[1],nimage.shape[0]))
    slika = np.hstack((slika, rotated))
    deg += 30

cv2.imwrite('C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab2\\slike\\tuttofinito.jpg', slika)
